Hargassner boiler to MQTT translator
====================================

Tool to fetch information from Hargassner heat boiler and pass
extracted the information to a MQTT broker.

The latest edition of the source can be found at
https://gitlab.com/petterreinholdtsen/hargassner2mqtt .

Please report any errors or improvements there.

The license is GNU General Public License version 2 or later at your
choice.


Supported models
----------------

Currently only using TCP connections to the boiler is implemented.
Connecting to port 23 return one text line with numbers every second
or so, and the list of numbers and their interpretation differ from
boiler control software version to version.

So far it has been tested on the following models and versions:

* `ECO-HK150 <https://www.hargassner.se/produkter/flis/pannor/eco-hk-150/eco-hk-150-med-ra150/>`_ with v16.0b1 firmware V1.0be


Hargassner related free software
--------------------------------

* Hargassner Monitor (German)
  https://github.com/bwurst/hargassner-monitor/
  
* Hargassner, PHP based web visualization of boiler state (French)
  https://github.com/Jahislove/Hargassner/

* Hargassner Python, Python script to write stats to mysql (English)
  https://github.com/Jahislove/hargassner-python
